FROM ubuntu:18.04
MAINTAINER Drew Stinnett <drew.stinnett@duke.edu>
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN pip install -r /app/requirements.txt
ENV FLASK_APP=app.py
ENTRYPOINT ["flask"]
CMD [ "run", "--host=0.0.0.0" ]
