from flask import Flask, request, render_template
import os
app = Flask(__name__)


@app.route('/')
def hello_world():
    proxy_ips = os.environ.get('PROXY_IPS').split(',')
    if request.headers.getlist("X-Forwarded-For"):
        remote_ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        remote_ip = request.remote_addr
    print("Connection from IP: %s" % remote_ip)
    if remote_ip in proxy_ips:
        using_proxy = True
    else:
        using_proxy = False

    return render_template('index.html.j2', context={
        'using_proxy': using_proxy,
        'ip': remote_ip
    })
